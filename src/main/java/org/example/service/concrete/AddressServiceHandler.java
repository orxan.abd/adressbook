package org.example.service.concrete;

import org.example.dao.entity.AddressEntity;
import org.example.service.abstraction.AddressOperationService;
import org.example.service.abstraction.ConnectionService;
import org.example.util.PropertiesUtil;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class AddressServiceHandler implements AddressOperationService {
    ConnectionService connectionService;

    public AddressServiceHandler(ConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Override
    public void insertToAddressBookTable(AddressEntity addressEntity) {
        try (var connect = connectionService.dbConnect();
             var insertToAddressBookTableStatement = connect.prepareStatement(PropertiesUtil.getKey("db.insertToAddressBook"))) {
            insertToAddressBookTableStatement.setString(1, addressEntity.getCountry().getGetCountryName());
            insertToAddressBookTableStatement.setString(2, addressEntity.getCity());
            insertToAddressBookTableStatement.setString(3, addressEntity.getDistrict());
            insertToAddressBookTableStatement.setInt(4, addressEntity.getHome());
            insertToAddressBookTableStatement.setInt(5, addressEntity.getApartment());
            insertToAddressBookTableStatement.setInt(6, addressEntity.getClientId());
            insertToAddressBookTableStatement.executeUpdate();
        } catch (SQLException f) {
            throw new RuntimeException(f);
        }
    }
}

