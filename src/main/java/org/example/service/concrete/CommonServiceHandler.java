package org.example.service.concrete;

import org.example.dao.entity.AddressEntity;
import org.example.dao.entity.PersonEntity;
import org.example.enums.Countries;
import org.example.service.abstraction.AddressOperationService;
import org.example.service.abstraction.CommonOperationService;
import org.example.service.abstraction.ConnectionService;
import org.example.util.PropertiesUtil;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CommonServiceHandler implements CommonOperationService {
    ConnectionService connectionService;
    AddressOperationService addressOperationService;


    public CommonServiceHandler(ConnectionService connectionService, AddressOperationService addressOperationService) {
        this.connectionService = connectionService;
        this.addressOperationService = addressOperationService;
    }

    private int insertToPersonInfoInt(String personName, String personSurname, int personAge) {
        try (var connect = connectionService.dbConnect();
             var insertToPersonInfoTableStatement = connect.prepareStatement(PropertiesUtil.getKey("db.insertToPersonInfo"), Statement.RETURN_GENERATED_KEYS)) {
            insertToPersonInfoTableStatement.setString(1, personName);
            insertToPersonInfoTableStatement.setString(2, personSurname);
            insertToPersonInfoTableStatement.setInt(3, personAge);
            insertToPersonInfoTableStatement.executeUpdate();
            var getGeneratedKeys = insertToPersonInfoTableStatement.getGeneratedKeys();
            return getGeneratedKeys.getInt("person_id");
        } catch (SQLException f) {
            throw new RuntimeException(f);
        }
    }

@Override
    public void insertToPersonInfoAndAddressBookTable(String personName, String personSurname, int personAge, AddressEntity addressEntity) {
        var clientId = insertToPersonInfoInt(personName, personSurname, personAge);
        addressEntity.setClientId(clientId);
        addressOperationService.insertToAddressBookTable(addressEntity);
    }

@Override
    public PersonEntity getFromPersonAndAddressTable(int personId) {
        try (var connect = connectionService.dbConnect();
             var getFromPersonAndAddressStatement = connect.prepareStatement(PropertiesUtil.getKey("db.selectFromAddressBook"))) {
            getFromPersonAndAddressStatement.setInt(1, personId);
            var getFromPersonAndAddressResult = getFromPersonAndAddressStatement.executeQuery();
            PersonEntity personEntity = null;
            List <AddressEntity> addressEntityList = new ArrayList<>();
            while (getFromPersonAndAddressResult.next()) {
                AddressEntity addressEntity = AddressEntity.builder()
                        .country(Countries.valueOf(getFromPersonAndAddressResult.getString(6)))
                        .city(getFromPersonAndAddressResult.getString(7))
                        .district(getFromPersonAndAddressResult.getString(8))
                        .home(getFromPersonAndAddressResult.getInt(9))
                        .apartment(getFromPersonAndAddressResult.getInt(10))
                        .clientId(getFromPersonAndAddressResult.getInt(11))
                        .build();
                 personEntity = PersonEntity.builder()
                         .personName(getFromPersonAndAddressResult.getString(2))
                         .personSurname(getFromPersonAndAddressResult.getString(3))
                         .personAge(getFromPersonAndAddressResult.getInt(4))
                         .build();
                 addressEntityList.add(addressEntity);
            }
            personEntity.setAddressEntityList(addressEntityList);
            return personEntity;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
