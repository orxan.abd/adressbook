package org.example.service.concrete;

import org.example.dao.entity.PersonEntity;
import org.example.service.abstraction.ConnectionService;
import org.example.service.abstraction.PersonOperationService;
import org.example.util.PropertiesUtil;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PersonServiceHandler implements PersonOperationService {
    ConnectionService connectionService;

    public PersonServiceHandler(ConnectionService connectionService) {
        this.connectionService = connectionService;
    }
@Override
    public void insertToPersonInfoTable(String personName, String personSurname, int personAge){
        try (var connect = connectionService.dbConnect();
             var insertToPersonInfoTableStatement = connect.prepareStatement(PropertiesUtil.getKey("db.insertToPersonInfo"))) {
            insertToPersonInfoTableStatement.setString(1, personName);
            insertToPersonInfoTableStatement.setString(2, personSurname);
            insertToPersonInfoTableStatement.setInt(3, personAge);
            insertToPersonInfoTableStatement.executeUpdate();
        } catch (SQLException exception) {
            throw new RuntimeException(exception);
        }
    }
    @Override
    public void getFromPersonTable(int personId){
        try (var connect = connectionService.dbConnect();
             var fromPersonTableStatement = connect.prepareStatement(PropertiesUtil.getKey("db.selectFromPersonInfo"))
        ) {
            fromPersonTableStatement.setInt(1, personId);
            var getFromPersonResult = fromPersonTableStatement.executeQuery();
            while (getFromPersonResult.next()) {
                PersonEntity.builder()
                        .personName(getFromPersonResult.getString(2))
                        .personSurname(getFromPersonResult.getString(3))
                        .personAge(getFromPersonResult.getInt(4))
                        .build();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
