package org.example.service.abstraction;

import org.example.dao.entity.PersonEntity;

public interface PersonOperationService {
    void insertToPersonInfoTable(String personName, String personSurname, int personAge);

    void getFromPersonTable(int personId);

}
