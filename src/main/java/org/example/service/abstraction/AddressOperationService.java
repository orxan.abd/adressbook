package org.example.service.abstraction;

import org.example.dao.entity.AddressEntity;

public interface AddressOperationService {

    void insertToAddressBookTable(AddressEntity addressEntity);

}
