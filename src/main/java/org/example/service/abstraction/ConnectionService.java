package org.example.service.abstraction;

import java.sql.Connection;

public interface ConnectionService {
    Connection dbConnect();

}
