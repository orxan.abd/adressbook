package org.example.service.abstraction;

import org.example.dao.entity.AddressEntity;
import org.example.dao.entity.PersonEntity;

public interface CommonOperationService {
    void insertToPersonInfoAndAddressBookTable(String personName, String personSurname, int personAge, AddressEntity addressEntity);

    PersonEntity getFromPersonAndAddressTable(int personId);
}
