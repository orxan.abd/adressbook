package org.example;

import org.example.service.concrete.AddressServiceHandler;
import org.example.service.concrete.CommonServiceHandler;
import org.example.service.concrete.PersonServiceHandler;
import org.example.util.ConnectionUtil;

public class Main {
    public static void main(String[] args) {
        ConnectionUtil connectionUtil = new ConnectionUtil();

        PersonServiceHandler personServiceHandler = new PersonServiceHandler(connectionUtil);
        AddressServiceHandler addressServiceHandler = new AddressServiceHandler(connectionUtil);
        CommonServiceHandler commonServiceHandler = new CommonServiceHandler(connectionUtil, addressServiceHandler);

        var data = commonServiceHandler.getFromPersonAndAddressTable(2);
        System.out.println(data);
//        data.getAddressEntityList().stream().forEach(System.out::println);

//        try {
//            commonServiceHandler.insertToPersonInfoAndAddressBookTable("Ork","Abd",13,Countries.AZERBAIJAN,
//                    "Bak","Xatai",45,45);
//        } catch (SQLException e) {
//            throw new RuntimeException(e);
//        }


    }
}