package org.example.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesUtil {
    static Properties properties = new Properties();

    static {
        propertiesLoad();
    }


    public static String getKey(String key) {
        return properties.getProperty(key);
    }

    public static void propertiesLoad() {
        var resourceAsStream = PropertiesUtil.class.getClassLoader().getResourceAsStream("application.properties");
        try {
            properties.load(resourceAsStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
