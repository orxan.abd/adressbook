package org.example.util;

import org.example.service.abstraction.ConnectionService;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtil implements ConnectionService {

    @Override
    public Connection dbConnect() {
        try {
            return DriverManager.getConnection(PropertiesUtil.getKey("db.url"),
                    PropertiesUtil.getKey("db.username"),
                    PropertiesUtil.getKey("db.password"));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
