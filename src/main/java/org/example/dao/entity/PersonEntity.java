package org.example.dao.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;
import java.util.logging.Level;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@ToString
@Builder
public class PersonEntity {
    String personName;
    String personSurname;
    int personAge;

    List<AddressEntity> addressEntityList;
}
