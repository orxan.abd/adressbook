package org.example.dao.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.example.enums.Countries;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@ToString
@Builder
public class AddressEntity {
    Countries country;
    String city;
    String district;
    int home;
    int apartment;
    int clientId;
}
